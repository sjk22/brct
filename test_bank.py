import sys
sys.path.append("../")
from bank import app
import pytest


test_case = {
    "right_name_format": "Sunjae Kim",
    "wrong_name_format": ["Sunja1 Kim", "S@mjae Kim"],
    "right_card_format": ["5157-7414-0497-8627", "5157741404978627"],
    "wrong_card_format": ["5157-7?14-0497-86a7", "5157741404978"],
    "right_pin_format": "1234",
    "wrong_pin_format": ["1sa4", "12345678"],
}


# Lets assume we already have two data in db
# id,  name,        card,              password,  balance
#  1,  Sunjae Kim,  3956723929903892,  1234,      0
#  2,  Kim Sunjae,  4215431138465733,  4321,      0
def test_verify():
    # check format of name
    assert app.verify(check='fmt', type='n', name=test_case["right_name_format"]) == True
    assert app.verify(check='fmt', type='n', name=test_case["wrong_name_format"][0]) == False
    assert app.verify(check='fmt', type='n', name=test_case["wrong_name_format"][1]) == False
    # check format of card
    assert app.verify(check='fmt', type='c', card=test_case["right_card_format"][0]) == True
    assert app.verify(check='fmt', type='c', card=test_case["right_card_format"][1]) == True
    assert app.verify(check='fmt', type='c', card=test_case["wrong_card_format"][0]) == False
    assert app.verify(check='fmt', type='c', card=test_case["wrong_card_format"][1]) == False
    # check format of pin
    assert app.verify(check='fmt', type='p', pin=test_case["right_pin_format"]) == True
    assert app.verify(check='fmt', type='p', pin=test_case["wrong_pin_format"][0]) == False
    assert app.verify(check='fmt', type='p', pin=test_case["wrong_pin_format"][1]) == False
    # check if name exists in db 
    assert app.verify(check='db', type='n', name='Sunjae Kim') == True
    assert app.verify(check='db', type='n', name='Seonjae Kim') == False
    # check if all the cards in db return as list 
    assert app.verify(check='db', type='c') == ['3956723929903892', '4215431138465733'] # card list
    # check if card number and pin is confirmed
    assert app.verify(check='db', type='p', card='4215431138465733', pin='4321') == 2
    assert app.verify(check='db', type='p', card='3956723929903892', pin='1234') == 1
    assert app.verify(check='db', type='p', card='4215431138465733', pin='1234') == None
    assert app.verify(check='db', type='p', card='3956723929903892', pin='432145') == None
    assert app.verify(check='db', type='p', card='395672392990389', pin='1234') == None 


def test_createNewAccount():
    # check if already exists in db
    assert app.createNewAccount(name='Sunjae Kim', pin='0000') == None
    # check format of input
    assert app.createNewAccount(name='Sunja! Kim', pin='0000') == None
    assert app.createNewAccount(name='Sunjae Kim', pin='000000') == None
    # check if new card has been made    
    new_card = app.createNewAccount(name='Sunjee Kim', pin='0000')
    assert app.verify(check='db', type='p', card=str(new_card), pin='0000') == 3
    assert app.checkBalance(card=str(new_card), pin='0000') == 0


def test_deposit():
    # deposit 100 dollars then check balance
    balance = app.deposit(card='3956723929903892', pin='1234', quantity='100')
    assert balance == 100
    assert app.checkBalance(card='3956723929903892', pin='1234') == 100
    # deposit 50 dollars then check balance
    balance = app.deposit(card='4215431138465733', pin='4321', quantity='50')
    assert balance == 50
    assert app.checkBalance(card='4215431138465733', pin='4321') == 50
    # check if cannot verify
    assert app.deposit(card='421543113846573', pin='4321', quantity='50') == None
    assert app.deposit(card='4215431138465733', pin='43210', quantity='50') == None
    assert app.deposit(card='4215431138465733', pin='1234', quantity='50') == None


def test_withdraw():
    # withdraw 100 dollars then check balance
    balance = app.withdraw(card='3956723929903892', pin='1234', quantity='100')
    assert balance == 0
    assert app.checkBalance(card='3956723929903892', pin='1234') == 0
    # withdraw 10 dollars then check balance
    balance = app.withdraw(card='4215431138465733', pin='4321', quantity='10')
    assert balance == 40
    assert app.checkBalance(card='4215431138465733', pin='4321') == 40
    # check if quantity to withdraw is over balance
    assert app.withdraw(card='4215431138465733', pin='4321', quantity='50') == None
    # check if cannot verify
    assert app.withdraw(card='421543113846573', pin='4321', quantity='1') == None
    assert app.withdraw(card='4215431138465733', pin='43210', quantity='1') == None
    assert app.withdraw(card='4215431138465733', pin='1234', quantity='1') == None


def test_checkBalance():
    # check balance
    assert app.checkBalance(card='3956723929903892', pin='1234') == 0
    assert app.checkBalance(card='4215431138465733', pin='4321') == 40
    # check if cannot verify
    assert app.checkBalance(card='421543113846573', pin='4321', quantity='1') == None
    assert app.checkBalance(card='4215431138465733', pin='43210', quantity='1') == None
    assert app.checkBalance(card='4215431138465733', pin='1234', quantity='1') == None


if __name__ == "__main__":
    sys.exit(pytest.main())