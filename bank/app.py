from typing import Union
from rich import print
from rich.prompt import Prompt
from rich.prompt import Confirm


def start() -> str:
    
    intro = """\nHello, welcome to our bank! How can I help you today?\n\n""" + \
    """\t(1) I want to build a new account\n""" + \
    """\t(2) I want to deposit money\n""" + \
    """\t(3) I want to withdraw money\n""" + \
    """\t(4) I want to check my current balance\n"""

    print(intro)

    choice = Prompt.ask("Choose one from the following", choices=["1", "2", "3", "4"])

    return choice


def verify(check='', type='', name='', card='', pin=''):
    '''
    check: 
        choose from 
            format (fmt), db (database)
    type: 
        choose from 
            card (c), name (n), pin (p)
    '''
    def isNameFormat(name: str):
        first_name, last_name = name.split()
        is_correct_first_name = not ' ' in first_name and not any(not c.isalpha() for c in first_name)
        is_correct_last_name = not ' ' in last_name and not any(not c.isalpha() for c in last_name)
        return is_correct_first_name and is_correct_last_name
    def isCardFormat(card: str):
        card = card.replace('-', '')
        return len(card) == 16 and not ' ' in card and not any(not c.isdecimal() for c in card)
    def isPinFormat(pin: str):
        return len(pin) == 4 and pin.isdecimal()

    if check in ("format", "fmt"):
        if type in ("name", "n"):
            return isNameFormat(name)
        if type in ("card", "c"):
            return isCardFormat(card)
        if type in ("pin", "p"):
            return isPinFormat(pin)
            
    if check in ("database", "db"):
        import sqlite3
        conn = sqlite3.connect("./data/client.db")
        cur = conn.cursor()

        if type in ("name", "n"):
            # See if there is already a registered name
            cur.execute("SELECT 1 FROM client_data WHERE client_data.name == '{}'".format(name))
            res = cur.fetchone()
            return True if res else False
            
        if type in ("card", "c"):
            # Get all the card number list to generate a unique card number
            conn.row_factory = lambda cursor, row: row[0]
            cur = conn.cursor()
            cur.execute("SELECT card FROM client_data")
            res = cur.fetchall()
            return res
            
        if type in ("pin", "p"):
            if card and pin:
                card = str(card).replace('-', '')
                if isCardFormat(card) and isPinFormat(str(pin)):
                    cur.execute("SELECT * FROM client_data WHERE card= ? and pin= ?", (card, pin))
                    f = cur.fetchone()
                    if f:
                        print('\nYour card has been confirmed.\n')
                        import time
                        time.sleep(1)
                        return f[0]
                    else:
                        print('\nOh no, either your card number or your PIN number is invalid. Please try again.')
                        return
                else:
                    print('\nOh no, either your card number or your PIN number is invalid. Please try again.')
                    return
            count = 0
            while True:
                if count > 3:
                    print("\nOops, you've tried too many times. Please try again later.")
                    return None
                card_message = """\nPlease enter your card number (16 digits)"""
                card = Prompt.ask(card_message)
                card = card.replace('-', '')
                pin_message = """Please enter the PIN number of your card"""
                pin = Prompt.ask(pin_message, password=True)
                count += 1
                if isCardFormat(card) and isPinFormat(pin):
                    cur.execute("SELECT * FROM client_data WHERE card= ? and pin= ?", (card, pin))
                    f = cur.fetchone()
                    if f:
                        print('\nYour card has been confirmed.\n')
                        import time
                        time.sleep(1)
                        return f[0]
                    else:
                        print('\nOh no, either your card number or your PIN number is invalid. Please try again.')
                        continue
                else:
                    print('\nOh no, either your card number or your PIN number is invalid. Please try again.')
                    continue
            
        conn.commit()
        cur.close()
        conn.close()                  
            
    
def createNewAccount(name='', card='', pin='', quantity=''):

    if not name:
        print("""\nThanks for selecting our bank! To start a new account, please first provide us with the following information.""")
        while True:
            print("Please provide your name with only alphabets, no digits or special signs such as '?'")
            first_name = Prompt.ask("\tFirst name")
            last_name = Prompt.ask("\tLast name")
            if verify(check="fmt", type="n", name=first_name+' '+last_name):
                is_correct_name = Confirm.ask("Is your name [{} {}]?".format(first_name.capitalize(), last_name.capitalize()))
                if is_correct_name:
                    if verify(check="db", type="n", name=first_name+' '+last_name):
                        print("It seems like your name is already registered. Please check again.\n")
                        continue
                    break
            print('Oops, wrong name format. Please check again.\n')
        name = first_name + ' ' + last_name
    
    if not verify(check="fmt", type="n", name=name):
        print("\nIt seems like your name is already registered. Please check again.")
        return
    
    if verify(check="db", type="n", name=name):
        print("\nIt seems like your name is already registered. Please check again.")
        return

    print('\nYour card is being generated. Please wait a moment.\n')
    
    ## DONE: check if card is in db
    import random
    card_list = verify(check='db', type='c')
    while True:
        new_card = random.randint(1e15, 1e16)
        if new_card not in card_list:
            break
    # new_card = random.choice(set(range(1e15, 1e16))-set(card_list))
    
    import time
    time.sleep(5)

    card_str_list = [str(new_card)[i:i+4] for i in range(0, 16, 4)]
    print('Your card number is [bold yellow]{}-{}-{}-{}'.format(*card_str_list))
    
    import sqlite3
    conn = sqlite3.connect("./data/client.db")
    cur = conn.cursor()

    if not pin:
        print('In order to activate your credit card, please provide with a PIN number.')
        while True:
            print('Your PIN must be 4 digits.')
            pin = Prompt.ask("\tPIN number", password=True)
            if not verify(check='fmt', type='p', pin=pin):
                print('Oops, wrong PIN format. Please check again.\n')
                continue
            confirm_pin = Prompt.ask("\tConfirm PIN number", password=True)
            if pin == confirm_pin:
                break
        
        print('\nPlease wait while we register your card number and PIN number.\n')
        time.sleep(3)
        
        cur.execute("INSERT INTO client_data(name, card, pin, balance) VALUES(?,?,?,?)", (name, new_card, pin, 0))
        conn.commit()
        cur.close()
        conn.close()

        print("\nCongratulations! You've just made a card account from our bank.\nWe look forward to giving you the best bank service. Have a nice day!")
        return

    cur.execute("INSERT INTO client_data(name, card, pin, balance) VALUES(?,?,?,?)", (name, new_card, pin, 0))
    
    conn.commit()
    cur.close()
    conn.close()
    
    return new_card


def deposit(name='', card='', pin='', quantity=''):
    if card and pin:
        id = verify(check="db", type="p", card=card, pin=pin)
    else:
        id = verify(check="db", type="p")
    if not id: return
    import sqlite3
    conn = sqlite3.connect("./data/client.db")
    cur = conn.cursor()
    if not quantity:
        quantity = Prompt.ask("How much money do you want to deposit (in dollars)")
        cur.execute("UPDATE client_data SET balance = balance + {} WHERE id = {}".format(quantity, id))
        cur.execute("SELECT balance FROM client_data WHERE id = {}".format(id))
        balance = cur.fetchone()[0]
        conn.commit()
        cur.close()
        conn.close()
        print("Your current balance is now [bold green3]{}[/] dollars. Have a nice day!".format(balance))
        return
    cur.execute("UPDATE client_data SET balance = balance + {} WHERE id = {}".format(quantity, id))
    cur.execute("SELECT balance FROM client_data WHERE id = {}".format(id))
    balance = cur.fetchone()[0]
    conn.commit()
    cur.close()
    conn.close()
    return balance


def withdraw(name='', card='', pin='', quantity=''):
    if card and pin:
        id = verify(check="db", type="p", card=card, pin=pin)
    else:
        id = verify(check="db", type="p")
    if not id: return
    import sqlite3
    conn = sqlite3.connect("./data/client.db")
    cur = conn.cursor()
    cur.execute("SELECT balance FROM client_data WHERE id = {}".format(id))
    if not quantity:
        quantity = Prompt.ask("How much money do you want to withdraw (in dollars)")
        cur.execute("SELECT balance FROM client_data WHERE id = {}".format(id))
        balance = cur.fetchone()[0]
        if int(balance) < int(quantity):
            print("Oops, you cannot withdraw more than your current balance, [bold green3]{}[/] dollars. Please try again later.".format(balance))
            return
        cur.execute("UPDATE client_data SET balance = balance - {} WHERE id = {}".format(quantity, id))
        cur.execute("SELECT balance FROM client_data WHERE id = {}".format(id))
        balance = cur.fetchone()[0]
        conn.commit()
        cur.close()
        conn.close()
        print("Your current balance is now [bold green3]{}[/] dollars. Have a nice day!".format(balance))
        return
    balance = cur.fetchone()[0]
    if int(balance) < int(quantity):
        print("Oops, you cannot withdraw more than your current balance, [bold green3]{}[/] dollars. Please try again later.".format(balance))
        return
    cur.execute("UPDATE client_data SET balance = balance - {} WHERE id = {}".format(quantity, id))
    cur.execute("SELECT balance FROM client_data WHERE id = {}".format(id))
    balance = cur.fetchone()[0]
    conn.commit()
    cur.close()
    conn.close()
    return balance


def checkBalance(name='', card='', pin='', quantity=''):
    import sqlite3
    if card and pin:
        id = verify(check="db", type="p", card=card, pin=pin)
        if not id: return
        conn = sqlite3.connect("./data/client.db")
        cur = conn.cursor()
        cur.execute("SELECT balance FROM client_data WHERE id = {}".format(id))
        balance = cur.fetchone()[0]
        conn.commit()
        cur.close()
        conn.close()
        return balance
    else:
        id = verify(check="db", type="p")
        if not id: return
        conn = sqlite3.connect("./data/client.db")
        cur = conn.cursor()
        cur.execute("SELECT balance FROM client_data WHERE id = {}".format(id))
        balance = cur.fetchone()[0]
        conn.commit()
        cur.close()
        conn.close()
        print("Your current balance is [bold green3]{}[/] dollars. Have a nice day!".format(balance))
        return


def main(test='', choice="1", name='', card='', pin='', quantity=''):
    
    func_dict = {
        "1": createNewAccount,
        "2": deposit,
        "3": withdraw,
        "4": checkBalance
    }

    if test == 'true':
        return func_dict["{}".format(choice)](name=name, card=card, pin=pin, quantity=quantity)
    else:
        choice = start()
        return func_dict[choice]()
    
