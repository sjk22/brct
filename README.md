## Definition
I redefined the problem so these four functions are necessary:

- A client can open a new card/account
- A client can withdraw money
- A client can deposit money
- A client can check their current balance

And of course, clients should beforehand verify themselves by inserting their card number and PIN.

## Run, Test environment
```
Windows 10 & Python v3.8.11
```

## Setup

```bash
$ python -m venv bankapp 
$ source bankapp/bin/activate # (for linux)
$ ./bankapp/Scripts/activate # (for windows)
$ pip install -r ./requirements.txt
```

## Run interactively
```bash
# start sqlite database
$ python ./misc/startdb.py
# do as many runs as you want
$ python -m bank
# before you exit terminal, please delete db
$ python ./misc/deletedb.py
```

## Run with command line
```bash
# start sqlite database
$ python ./misc/startdb.py
# make new account
$ python -m bank --test="true" --choice="1" --name="Bear Robotics" --pin="2022"
# deposit
$ python -m bank --test="true" --choice="2" --card="<card # after making new account>" --pin="2022" --quantity='55'
# withdraw
$ python -m bank --test="true" --choice="3" --card="<card # after making new account>" --pin="2022" --quantity='25'
# check current balance
$ python -m bank --test="true" --choice="4" --card="<card # after making new account>" --pin="2022"
# delete db
$ python ./misc/deletedb.py
```

## Test app
```bash
# start sqlite database
$ python ./misc/startdb.py
# insert test data
$ python ./misc/testdb.py
# run tests
$ python ./test_bank.py
# before you exit terminal, please delete db
$ python ./misc/deletedb.py
```


## To be considered for integration and development
- shebangs for easier execution
- transaction (bc it was not the requirements)
- Concurrent & Event-driven daemon service
- Scalability (sqlite -> MySQL or NoSQL)
- Security of application
- Strict type check
- Io bound
- etc. 
