f = open('./data/client.db', 'w+')
f.close()

import sqlite3

# conn = sqlite3.connect("./data/client.db")
conn = sqlite3.connect("./data/client.db")
cur = conn.cursor()

cur.execute('CREATE TABLE IF NOT EXISTS client_data(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name TEXT, card TEXT, pin TEXT, balance INTEGER)')

# cur.execute('SELECT 1 FROM "hello" WHERE "world" == 1')
# cur.execute("SELECT * FROM sth WHERE card='1234567812345678' and password='123456677'")

conn.commit()
conn.close()