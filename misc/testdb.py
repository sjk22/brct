import sqlite3

conn = sqlite3.connect('./data/client.db')
c = conn.cursor()

#records or rows in a list
records = [('Sunjae Kim', '3956723929903892', '1234', 0,),
			('Kim Sunjae', '4215431138465733', '4321', 0,)]

#insert multiple records in a single query
c.executemany('INSERT INTO client_data(name, card, pin, balance) VALUES(?,?,?,?);', records)

print('We have inserted', c.rowcount, 'records to the table.')

#commit the changes to db			
conn.commit()
#close the connection
conn.close()